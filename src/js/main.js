class Modal {

    constructor(modal, machine, close, form, message) {
        this.modal = document.querySelector(modal);
        this.machine = document.querySelector(machine);
        this.close = document.querySelector(close);
        this.form = document.querySelector(form);
        this.message = document.querySelector(message);
    }

    init() {
        this.bindEvents();
    }

    bindEvents() {
        this.machine.addEventListener('click', this.handleClick.bind(this));
        this.close.addEventListener('click', this.closePopup.bind(this));
        this.form.addEventListener('submit', this.handleSubmission.bind(this));
    }

    handleClick(e) {
        if (e.target.nodeName === 'BUTTON') {
            e.preventDefault();
            this.openPopup();
        }
    }

    openPopup() {
        this.modal.classList.add('is-active');
    }

    closePopup() {
        this.modal.classList.remove('is-active');
    }

    handleSubmission(e) {
        if (e) e.preventDefault();

        if (this.simulateSendLink() === true) {
            this.message.innerHTML = 'Game should appear on your mobile soon.';
        } else {
            this.message.innerHTML = 'Something went wrong, please try again.';
        }
    }

    simulateSendLink() {
        return Math.random() >= 0.5;
    }

}

function app() {
    const modal = new Modal('.js-modal', '.js-machine', '.js-modal-close', '.js-form', '.js-modal-message');
    modal.init();
}

window.addEventListener('load', function () {
    app();
});