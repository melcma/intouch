I fetch API with curl and save it in local JSON file for performance reasons.
Function updateData() in GamesAPI class checks the response status and updates local JSON if file is older than 10 minutes.

Frontend uses SASS in SCSS syntax, BEM, SMACSS and OOCSS methodologies, plain JavaScript and all is bundled with webpack.

Simulated functionality for "Send to your phone" prompt that returns randomly success or failure.

Developed in PHPStorm, Ubuntu, Firefox Developer Edition.

Tested in:

Windows 10 Chrome
Windows 10 Firefox
iOS Safari
MacOS Chrome
Android Firefox
Android Chrome
