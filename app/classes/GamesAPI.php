<?php

class GamesAPI
{

    public $url;
    public $file;

    function __construct($url, $file)
    {
        $this->url = $url;
        $this->file = $file;
    }

    private function readFromFile()
    {
        $fh = fopen($this->file, 'r');

        return json_decode(stream_get_contents($fh));
    }

    private function saveToFile()
    {
        $data = $this->fetchData();

        if ($data !== false) {
            $fh = fopen($this->file, 'w+');
            chmod($this->file, 0775);
            fwrite($fh, $this->fetchData());
            fclose($fh);
            return true;
        }

        return false;

    }

    private function getFileTimestamp()
    {
        if (file_exists($this->file)) {
            return filemtime($this->file);
        }

        return false;
    }

    private function updateData()
    {
        if (!file_exists($this->file) or time() - $this->getFileTimestamp() > 600) {
            $this->saveToFile();
        }
    }

    public function fetchData()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        $result = curl_exec($ch);
        $response_status = curl_getinfo($ch)['http_code'];

        curl_close($ch);

        if ($response_status === 200) {
            return $result;
        }

        return false;

    }

    public function getGames($games_number = NULL)
    {
        $this->updateData();

        $games = $this->readFromFile();
        $games = array_slice($games, 0, $games_number);

        return $games;
    }
}