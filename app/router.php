<?php

$request = $_SERVER['REQUEST_URI'];
$view = null;

switch ($request) {
    case '/':
        require_once '../public/views/index.php';
        break;
    default:
        require_once '../public/views/404.php';
        break;
}