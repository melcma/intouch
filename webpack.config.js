const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const extractText = new ExtractTextPlugin({filename: 'main.css'});
const env = process.env.NODE_ENV;

const plugins = {
    'production': [
        extractText
    ],
    'development': [
        extractText,
        new BrowserSyncPlugin({
            proxy: 'intouch.local',
            host: 'localhost',
            port: '9006',
            files: ['./src/**/*.scss']
        })]
};

module.exports = {
    watch: env === 'development',
    entry: {
        app: ['./src/app.js']
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: extractText.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true
                            }

                        }, {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true
                            }

                        }, {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        }]
                })
            },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }
        ]
    },
    plugins: plugins[env]
};