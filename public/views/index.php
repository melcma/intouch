<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Intouch Recruitment App Description">
    <meta name="robots" content="all">

    <title>Intouch Recruitment App</title>

    <link href="main.css" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet">
</head>

<body>

<?php include 'components/machine.php'; ?>
<?php include 'components/modal.php'; ?>

<script src="bundle.js" type="application/javascript" rel="script"></script>

</body>

</html>