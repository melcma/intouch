<section class="container">

    <div class="machine  flex  mq-tiny--flex  js-machine">

        <?php
        $games_number = $config['games_number'];
        $gamesAPI = new GamesAPI($config['gamesAPIUrl'], $config['gamesAPIFile']);
        $games = $gamesAPI->getGames($games_number);
        $games_columns = partition($games, 3);
        ?>

        <?php foreach ($games_columns as $index => $games): ?>

            <div class="col  col-4  col-tiny-12  col--flush-small-4  machine__column  machine__column--<?= $index + 1; ?>">

                <div class="machine__inner">

                    <div class="machine__games">

                        <?php foreach ($games as $game): ?>

                            <div class="game">

                                <img src="<?= $game->gameImage; ?>"
                                     class="img  img--responsive"
                                     alt="<?= $game->gameName; ?>">

                                <div class="game__overlay">

                                    <div class="game__details">

                                        <p class="mt0  mb15">
                                            Jackpot:<br>
                                            &pound;<?= number_format($game->gameJackpot, 2); ?>
                                        </p>

                                        <a href="<?= $game->gamePageURL; ?>"
                                           target="_blank"
                                           rel="noopener"
                                           title="<?= $game->gameName; ?> more info"
                                           class="btn  btn--info  mb5">More info</a>

                                        <button title="Send <?= $game->gameName; ?> to mobile"
                                                class="btn  btn--send">Send to mobile
                                        </button>

                                    </div>

                                </div>

                            </div>

                        <?php endforeach; ?>

                    </div>

                </div>

            </div>

        <?php endforeach ?>

        <div class="machine__beam"></div>

        <div class="clearfix"></div>

    </div>

</section>