<div class="modal  js-modal">

    <div class="modal__box">

        <button class="modal__close  js-modal-close">x</button>

        <p class="mt5">Please enter your phone number:</p>

        <form class="js-form">
            <input type="text"
                   class="modal__input  js-modal-phone"
                   name="phone"
                   placeholder="+44 000 000 000"
                   required>
            <input type="submit"
                   class="modal__submit  js-modal-submit"
                   value="Send">
        </form>

        <p class="modal__message  mt30  js-modal-message"></p>

    </div>

</div>